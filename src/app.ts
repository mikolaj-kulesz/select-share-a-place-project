import axios from 'axios'

type GoogleGeocodeResponse = {
    results: {
        geometry: {
            location: {
                lat: number,
                lng: number,
            }
        }
    }[],
    status: 'OK' | 'ZERO_RESULTS'
}

const form = document.querySelector('form')!
const addressInput = document.getElementById('address')! as HTMLInputElement
const GOOGLE_API_KEY = 'AIzaSyAyutDntYdX-eWBsDvpziJdRMjZXPH13Uw'
const searchAddressHandler = (event: Event) => {
    event.preventDefault()
    const address = addressInput.value
    axios
        .get<GoogleGeocodeResponse>(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(address)}&key=${GOOGLE_API_KEY}`)
        .then(res => {
            const {status} = res.data
            if(status !== 'OK') throw Error('Could not find location :(')
            const coordinates = res.data.results[0].geometry.location
            const map = new google.maps.Map(document.getElementById('map')!, {
                center: coordinates,
                zoom: 12
            });
            new google.maps.Marker({position: coordinates, map: map});
        })
        .catch(err => {
            console.error('err', err)
        })
}

form.addEventListener('submit', searchAddressHandler)
